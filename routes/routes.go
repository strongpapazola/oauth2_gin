package routes

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"oauth2/controller"

	"gopkg.in/oauth2.v3/models"

	"gopkg.in/oauth2.v3/errors"
	"gopkg.in/oauth2.v3/manage"
	"gopkg.in/oauth2.v3/server"
	"gopkg.in/oauth2.v3/store"
)

// func GetAllUser(c *gin.Context) {
// 	// Get DB from Mongo Config
// 	db := conn.GetMongoDB()
// 	users := user.Users{}
// 	err := db.C(UserCollection).Find(bson.M{}).All(&users)
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, gin.H{"status": "failed", "message": errNotExist.Error()})
// 		return
// 	}
// 	c.JSON(http.StatusOK, gin.H{"status": "success", "users": &users})
// }

func StartAuth() {
	manager := manage.NewDefaultManager()
	manager.SetAuthorizeCodeTokenCfg(manage.DefaultAuthorizeCodeTokenCfg)
	manager.MustTokenStorage(store.NewMemoryTokenStore())
	clientStore := store.NewClientStore()
	manager.MapClientStorage(clientStore)
	srv := server.NewDefaultServer(manager)
	srv.SetAllowGetAccessRequest(true)
	srv.SetClientInfoHandler(server.ClientFormHandler)
	manager.SetRefreshTokenCfg(manage.DefaultRefreshTokenCfg)

	srv.SetInternalErrorHandler(func(err error) (re *errors.Response) {
		log.Println("Internal Error:", err.Error())
		return
	})

	srv.SetResponseErrorHandler(func(re *errors.Response) {
		log.Println("Response Error:", re.Error.Error())
	})

	http.HandleFunc("/oauth2/token", func(w http.ResponseWriter, r *http.Request) { srv.HandleTokenRequest(w, r) })
	http.HandleFunc("/oauth2/credentials", func(w http.ResponseWriter, r *http.Request) {
		// clientId := uuid.New().String()[:8]
		// clientSecret := uuid.New().String()[:8]
		clientId := "00000"
		clientSecret := "99999"
		err := clientStore.Set(clientId, &models.Client{
			ID:     clientId,
			Secret: clientSecret,
			Domain: "http://localhost:9094",
		})
		if err != nil {
			fmt.Println(err.Error())
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(map[string]string{"client_id": clientId, "client_secret": clientSecret})
	})

	// http.HandleFunc("/oauth2/protected", validateToken(protected, srv))
	// http.HandleFunc("/oauth2/register", Register)
	http.HandleFunc("/oauth2/protected", validateToken(controller.Protected, srv))
	http.HandleFunc("/oauth2/proxy", validateToken(controller.Proxy, srv))
	http.HandleFunc("/oauth2/getid", validateToken(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(srv.ClientInfoHandler)
	}, srv))

	http.HandleFunc("/api/users", controller.Proxy).Method()
	http.HandleFunc("/api/users", controller.Proxy)
	http.HandleFunc("/api/users/{id}", controller.Proxy)
	http.HandleFunc("/api/users/:id", controller.Proxy)
	http.HandleFunc("/api/users/:id", controller.Proxy)

	log.Println("[*] Web Server Started On 9096...")
	log.Fatal(http.ListenAndServe(":9096", nil))
}

func validateToken(f http.HandlerFunc, srv *server.Server) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// ua := r.Header.Get("X-API-KEY")
		_, err := srv.ValidationBearerToken(r)

		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		f.ServeHTTP(w, r)
	})
}
