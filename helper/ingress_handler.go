package helper

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func Wrapper(f func(c *gin.Context) (string, error), msg string) gin.HandlerFunc {
	return func(c *gin.Context) {
		_, err := f(c)
		if err != nil {
			c.JSON(503, gin.H{"status": err})
			return
		}
		c.JSON(200, gin.H{"status": "OK"})
	}
}

func Proxya(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": c.Request.URL.Path,
	})
	endpointRoot := strings.Split(c.Request.URL.Path, "/")[1]
	endpointPath := strings.Join(strings.Split(c.Request.URL.Path, "/")[2:], "_")
	fmt.Println(endpointRoot + "_" + endpointPath)
}

func Proxy(c *gin.Context, url string) {
	req, err := http.NewRequest(c.Request.Method, url, c.Request.Body)
	if err != nil {
		log.Println(err.Error)
		c.String(ErrorCode(500, "Fail Set Requests"+err.Error()))
	}
	req.Header.Add("Content-Type", c.Request.Header.Get("Content-Type"))
	req.Header.Add("X-API-KEY", c.Request.Header.Get("X-API-KEY"))
	req.Header.Add("Authorization", c.Request.Header.Get("Authorization"))

	client := &http.Client{}
	responseServer, err := client.Do(req)
	if err != nil {
		log.Println(err)
		c.String(ErrorCode(500, "Fail Hit Requests"+err.Error()))
	}
	responseClientBody, err := ioutil.ReadAll(responseServer.Body)
	if err != nil {
		log.Println(err)
		c.String(ErrorCode(500, "Fail Read Response Server"+err.Error()))
	}

	c.Writer.Header().Set("Content-Type", responseServer.Header.Get("Content-Type")) // set responseclientheader
	c.String(http.StatusOK, BytesToString(responseClientBody))
}

func BytesToString(data []byte) string {
	return string(data[:])
}

func geturi(w http.ResponseWriter, r *http.Request) string {
	host := ".default.srv.cluster.local"
	if r.URL.RawQuery != "" {
		return "https://" + host + r.URL.Path + "?" + r.URL.RawQuery
	} else {
		return "https://" + host + r.URL.Path
	}
}

// func Proxyo(w http.ResponseWriter, r *http.Request) {
// 	req, err := http.NewRequest(r.Method, geturi(w, r), r.Body)
// 	if err != nil {
// 		log.Println(err.Error)
// 		w.Write(ErrorCode(500, "Fail Set Requests"+err.Error()))
// 	}
// 	req.Header.Add("Content-Type", r.Header.Get("Content-Type"))
// 	req.Header.Add("X-API-KEY", r.Header.Get("X-API-KEY"))
// 	req.Header.Add("Authorization", r.Header.Get("Authorization"))

// 	client := &http.Client{}
// 	responseServer, err := client.Do(req)
// 	if err != nil {
// 		log.Println(err)
// 		w.Write(ErrorCode(500, "Fail Hit Requests"+err.Error()))
// 	}
// 	responseClientBody, err := ioutil.ReadAll(responseServer.Body)
// 	if err != nil {
// 		log.Println(err)
// 		w.Write(ErrorCode(500, "Fail Read Response Server"+err.Error()))
// 	}

// 	w.Write(responseClientBody)
// 	w.Header().Set("Content-Type", responseServer.Header.Get("Content-Type")) // set responseclientheader
// 	w.WriteHeader(responseServer.StatusCode)
// }
