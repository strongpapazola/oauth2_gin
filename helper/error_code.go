package helper

import "net/http"

func ErrorCode(num int, msg string) (int, string) {
	if msg != "" {
		if num == 400 {
			return http.StatusBadRequest, "{\"error\":400,\"msg\":\"" + msg + "\"}"
		} else if num == 403 {
			return http.StatusForbidden, "{\"error\":403,\"msg\":\"" + msg + "\"}"
		} else if num == 500 {
			return http.StatusInternalServerError, "{\"error\":500,\"msg\":\"" + msg + "\"}"
		}
		return http.StatusBadGateway, "Error Response" + msg
	} else {
		if num == 400 {
			return http.StatusBadRequest, "{\"error\":400,\"msg\":\"Bad Request\"}"
		} else if num == 403 {
			return http.StatusForbidden, "{\"error\":403,\"msg\":\"Forbidden\"}"
		} else if num == 500 {
			return http.StatusInternalServerError, "{\"error\":500,\"msg\":\"Internal Server Error\"}"
		}
		return http.StatusBadGateway, "Error Response"
	}
}

func ErrorCodeold(num int, msg string) []byte {
	if msg != "" {
		if num == 400 {
			return []byte("{\"error\":400,\"msg\":\"" + msg + "\"}")
		} else if num == 403 {
			return []byte("{\"error\":403,\"msg\":\"" + msg + "\"}")
		} else if num == 500 {
			return []byte("{\"error\":500,\"msg\":\"" + msg + "\"}")
		}
		return []byte("Error Response")
	} else {
		if num == 400 {
			return []byte("{\"error\":400,\"msg\":\"Bad Request\"}")
		} else if num == 403 {
			return []byte("{\"error\":403,\"msg\":\"Forbidden\"}")
		} else if num == 500 {
			return []byte("{\"error\":500,\"msg\":\"Internal Server Error\"}")
		}
		return []byte("Error Response")
	}
}
