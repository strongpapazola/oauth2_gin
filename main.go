package main

import (
	"net/http"
	"oauthgin/helper"

	"github.com/gin-gonic/gin"
	ginserver "github.com/go-oauth2/gin-server"
	"gopkg.in/oauth2.v3/manage"
	"gopkg.in/oauth2.v3/models"
	"gopkg.in/oauth2.v3/server"
	"gopkg.in/oauth2.v3/store"
)

func main() {
	manager := manage.NewDefaultManager()

	// token store
	manager.MustTokenStorage(store.NewFileTokenStore("data.db"))

	// client store
	clientStore := store.NewClientStore()
	clientStore.Set("000000", &models.Client{
		ID:     "000000",
		Secret: "999999",
		Domain: "http://localhost",
	})
	manager.MapClientStorage(clientStore)

	// Initialize the oauth2 service
	ginserver.InitServer(manager)
	ginserver.SetAllowGetAccessRequest(true)
	ginserver.SetClientInfoHandler(server.ClientFormHandler)

	g := gin.Default()

	auth := g.Group("/oauth2")
	{
		auth.GET("/token", ginserver.HandleTokenRequest)
	}

	api := g.Group("/api")
	{
		api.Use(ginserver.HandleTokenVerify())
		api.GET("/test", func(c *gin.Context) {
			ti, exists := c.Get(ginserver.DefaultConfig.TokenKey)
			if exists {
				c.JSON(http.StatusOK, ti)
				return
			}
			c.String(http.StatusOK, "not found")
		})
		api.GET("/users", users_gets)
		api.POST("/users", users_create)
		api.GET("/users/:id", users_get)
		api.PUT("/users/:id", users_update)
		api.DELETE("/users/:id", users_delete)
	}

	g.Run(":9096")
}

func users_gets(c *gin.Context) {
	service := "http://usersget-service.default.svc.cluster.local/api/users"
	helper.Proxy(c, service)
}
func users_create(c *gin.Context) {
	service := "http://userscreate-service.default.svc.cluster.local/api/users"
	helper.Proxy(c, service)
}
func users_get(c *gin.Context) {
	service := "http://usersget-service.default.svc.cluster.local/api/users/" + c.Param("id")
	helper.Proxy(c, service)
}
func users_update(c *gin.Context) {
	service := "http://usersupdate-service.default.svc.cluster.local/api/users/" + c.Param("id")
	helper.Proxy(c, service)
}
func users_delete(c *gin.Context) {
	service := "http://usersdelete-service.default.svc.cluster.local/api/users/" + c.Param("id")
	helper.Proxy(c, service)
}

func protected(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"Test": "test"})
}
